BEGIN	{
	printf("graph {\n\trankdir=LR;\n");
}
{
	
	if($1 == "Package:"){
		#print $2;
		name = $2;
		
	}

	if($1 == "Depends:"){
		printf("\tsubgraph \"%s\" { \n\t\tlabel=\"%s\"\n",name,name);
		for(i=2; i <= NF; i++){
			if ($i !~ /\(/){
				if ($i !~ /\)/){
					if ($i !~ /\|/){
						#out[i] = $i;
						#print out[i];
						printf("\t\t\"%s\" -- \"%s\";\n", $i, name);
					}
				}
			}
		
		}
		printf("\t}\n");
	}
	
}

END	{
	print "}";
}


